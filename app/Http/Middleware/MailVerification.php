<?php

namespace App\Http\Middleware;

use Closure;

class MailVerification
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->user() && !$request->user()->hasVerifiedEmail() && !$request->user()->isRegisterSocialNetworkingSites()){
            return redirect('auth/mail/verify-page');
        }
        else{
            return $next($request);
        }
    }
}
