<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Redirect;

class SmsVerification
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->user() && !$request->user()->hasVerifiedSms() && !$request->user()->isRegisterSocialNetworkingSites()){
            return redirect('auth/sms/verify-page');
        }
        else{
            return $next($request);
        }
    }
}
