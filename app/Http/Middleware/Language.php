<?php

namespace App\Http\Middleware;

use Closure;

class Language
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $aviableLanguages = ['az','ru','en'];
        $userLangs = preg_split('/,|;/', $request->server('HTTP_ACCEPT_LANGUAGE'));
        if (in_array($userLangs[0],$aviableLanguages)){
            app()->setLocale($userLangs[0]);
        }
        return $next($request);
    }
}
