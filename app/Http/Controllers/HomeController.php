<?php

namespace App\Http\Controllers;

use App\Jobs\SendSmsJob;
use App\Mail\VerifyMail;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth','mail','sms']);
    }

    public function index(){
        return view('welcome');
    }
}
