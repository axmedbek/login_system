<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    public function showLoginForm()
    {
        return view('auth.login');
    }


    public function logout(Request $request)
    {
        $this->guard()->logout();
        $request->session()->invalidate();
        return redirect()->back();
    }

    public function login(Request $request)
    {
        $requestParam = "username";
        if (filter_var($request->get('username'),FILTER_VALIDATE_EMAIL)){
            $requestParam = "email";
        }

        auth()->attempt([
            $requestParam => $request->get('username'),
            'password' => $request->get('password')],
            $request->filled('remember'));

        if (auth()->check()) {
            return redirect()->intended('/');
        }
        return redirect()->back()->withErrors([
            'error' => __('auth.login_error')
        ]);
    }
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
}
