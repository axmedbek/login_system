<?php

namespace App\Http\Controllers\Auth;

use App\Jobs\MailSenderJob;
use App\Jobs\SendSmsJob;
use Carbon\Carbon;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Foundation\Auth\RedirectsUsers;
use Illuminate\Foundation\Auth\VerifiesEmails;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MailVerificationController extends Controller
{
    //use RedirectsUsers;
    //use VerifiesEmails;

    protected $redirectTo = "/";

//    public function __construct()
//    {
//        $this->middleware('mail');
//    }

    public function show(Request $request){
        return $request->user()->hasVerifiedEmail()
        || $request->user()->isRegisterSocialNetworkingSites()
            ? redirect($this->redirectTo)
            : view('auth.verify');
    }

    public function verify(Request $request){
        if ($request->get('id') != $request->user()->getKey()) {
            throw new AuthorizationException;
        }

        if ($request->user()->hasVerifiedEmail() || $request->user()->isRegisterSocialNetworkingSites()) {
            return redirect($this->redirectTo);
        }

//        make verified
        $request->user()->email_verified_at = Carbon::now();
        $request->user()->save();

        $accountSid = config('app.twilio')['TWILIO_ACCOUNT_SID'];
        $authToken = config('app.twilio')['TWILIO_AUTH_TOKEN'];

        SendSmsJob::dispatch($request->user(),$accountSid,$authToken);
        return redirect($this->redirectTo);
    }

    public function resend(Request $request){
        if ($request->user()->hasVerifiedEmail() || $request->user()->isRegisterSocialNetworkingSites()) {
            return redirect($this->redirectTo);
        }
        MailSenderJob::dispatch($request->user());
        return redirect()->back();
    }
}
