<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SmsVerificationController extends Controller
{
    protected $redirectTo = "/";

//    public function __construct()
//    {
//        $this->middleware('sms');
//    }

    public function verifySms(Request $request){

        if ($request->user()->hasVerifiedSms()
            || $request->user()->isRegisterSocialNetworkingSites()
        ) {
            return redirect($this->redirectTo);
        }

        $code = implode($request->get('number'),'');
        $user = $request->user();

        if ($user && $user->sms_verification_code == $code){
            $user->phone_number_verified_at = Carbon::now();
            $user->save();
            return redirect('/');
        }
        else{
            return redirect()->back();
        }
    }

    public function smsPage(Request $request){
        return $request->user()->hasVerifiedSms() || $request->user()->isRegisterSocialNetworkingSites()
            ? redirect($this->redirectTo)
            : view('auth.sms');
    }
}
