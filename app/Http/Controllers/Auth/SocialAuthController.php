<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Laravel\Socialite\Facades\Socialite;

class SocialAuthController extends Controller
{
    protected $redirectTo = "/";
    private $userObj;

    public function __construct(User $userObj)
    {
        $this->userObj = $userObj;
    }

    public function redirectToProvider($provider)
    {
        return Socialite::driver($provider)->redirect();
    }

    public function handleProviderCallback($provider)
    {
        try{
            $user = Socialite::driver($provider)->user();
            $authUser = $this->findOrCreateUser($user, $provider);
            if ($authUser['status']){
                auth()->login($authUser['user'], true);
                return redirect($this->redirectTo);
            }
            else{
                return redirect()->back()->withErrors(['error' => $authUser['error']]);
            }
        }
        catch (\Exception $exception){
            return redirect()->back()->withErrors(['error' => $exception->getMessage()]);
        }
    }
    public function findOrCreateUser($user, $provider)
    {
        $authenticatedUser = $this->userObj::where('provider_id', $user->id)->first();

        if ($authenticatedUser) {
            return ['status' => true , 'user' => $authenticatedUser];
        }
        try{
            $user =  $this->userObj::create([
                'full_name'   => $user->name,
                'email'       => $user->email,
                "avatar"      => $user->avatar,
                'provider'    => $provider,
                'provider_id' => $user->id
            ]);

            return ['status' => true , 'user' => $user];
        }
        catch (\Exception $exception){
            return ['status' => false , 'error' => 'This email already using another account'];
        }
    }
}
