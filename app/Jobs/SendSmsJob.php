<?php

namespace App\Jobs;

use App\Mail\VerifyMail;
use App\User;
use GuzzleHttp\Client;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Log;
use Illuminate\Support\Facades\Mail;

class SendSmsJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $tries = 5;
    protected $user;
    protected $accountSid;
    protected $authToken;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($user,$accountSid,$authToken)
    {
        $this->user = $user;
        $this->accountSid = $accountSid;
        $this->authToken = $authToken;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $phone_number = str_replace(['(',')','-',' '],'',$this->user->phone_number);
        $code = $this->user->sms_verification_code;

        $client = new Client(['auth' => [$this->accountSid, $this->authToken]]);
        $client->post('https://api.twilio.com/2010-04-01/Accounts/'.$this->accountSid.'/Messages.json',
                ['form_params' => [
                    'Body' => 'CODE: '. $code, //set message body
                    'To' => $phone_number,
                    'From' => '+17722141658' //we get this number from twilio
                ]]);

    }
}
