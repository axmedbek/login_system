<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'full_name',
        'username',
        'email',
        'phone_number',
        'password',
        'provider',
        'provider_id',
        'email_verification_code',
        'sms_verification_code'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function hasVerifiedEmail()
    {
        return ! is_null($this->email_verified_at);
    }

    public function hasVerifiedSms(){
        return !is_null($this->phone_number_verified_at);
    }

    public function isRegisterSocialNetworkingSites(){
        return !is_null($this->provider);
    }
}
