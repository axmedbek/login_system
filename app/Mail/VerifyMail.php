<?php

namespace App\Mail;

use App\User;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\URL;

class VerifyMail extends Mailable
{
    use Queueable, SerializesModels;

    protected $user;

    /**
     * Create a new message instance.
     *
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('auth.email')->with([
            'user' => $this->user,
            'url' => $this->verificationUrl($this->user->getKey())]);
    }

    protected function verificationUrl($key)
    {
        return URL::temporarySignedRoute(
            'auth.mail.verify', Carbon::now()->addMinutes(60), ['id' => $key]
        );
    }

}
