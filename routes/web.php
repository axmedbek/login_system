<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('auth/logout', 'Auth\LoginController@logout')->name('auth.logout');

Route::group(['middleware' => 'lang'], function () {
    Route::group(['prefix' => 'authentication', 'middleware' => ['guest:web']], function () {
        Route::get('login', 'Auth\LoginController@showLoginForm')->name('auth.login.get');
        Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('auth.register.get');
        Route::post('login', 'Auth\LoginController@login')->name('auth.login.post');
        Route::post('register', 'Auth\RegisterController@register')->name('auth.register');
        Route::get('{provider}', 'Auth\SocialAuthController@redirectToProvider')->name('social.register');
        Route::get('{provider}/callback', 'Auth\SocialAuthController@handleProviderCallback');
        Route::get('verify/{token}', 'Auth\RegisterController@verifyEmail')->name('mail.verify');
    });

    Route::group(['middleware' => ['auth:web']], function () {
        Route::group(['prefix' => 'auth'], function () {
            Route::get('sms/verify-page', 'Auth\SmsVerificationController@smsPage')->name('auth.sms.page');
            Route::post('send/sms', 'Auth\SmsVerificationController@sendSms')->name('auth.send.sms');
            Route::post('verify/sms', 'Auth\SmsVerificationController@verifySms')->name('auth.sms.verify');
            Route::get('mail/verify-page', 'Auth\MailVerificationController@show')->name('auth.mail.page');
            Route::get('mail/resend', 'Auth\MailVerificationController@resend')->name('auth.mail.resend');
            Route::get('mail/verify/{token}', 'Auth\MailVerificationController@verify')->name('auth.mail.verify');
        });
        Route::get('/', 'HomeController@index')->name('home');
    });
});
