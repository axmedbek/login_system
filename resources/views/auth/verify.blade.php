<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Email Verification</title>
    <style>
        div.verify-text{
            text-align: center;
            margin-top: 20%;
            font-size: 25px;
            padding: 30px 15px 15px 15px;
            background-color: aquamarine;
            width: 50%;
            position: absolute;
            left: 25%;
            border-radius: 4px;
            box-shadow: 5px 6px #b3abab;
            font-family: sans-serif;
        }
        div>button{
            padding: 10px;
            border-radius: 4px;
            background-color: antiquewhite;
            font-family: monospace;
        }
    </style>
</head>
<body>
    <div class="verify-text">
        Please check your email address {{
        str_repeat("*",strlen(auth()->user()->email) - 10).substr(auth()->user()->email,-10) }}
        <hr>
        <div>
            <button>
                <a href="{{ route('auth.mail.resend') }}" style="text-decoration: none;">
                    Resend mail
                </a>
            </button>
        </div>
    </div>
</body>
</html>
