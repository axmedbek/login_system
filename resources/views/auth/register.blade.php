<!DOCTYPE html>
<html lang="en">
<head>
    <title>Login System</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="{{ asset('vendor/bootstrap/css/bootstrap.min.css') }}">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{ asset('fonts/font-awesome-4.7.0/css/font-awesome.min.css') }}">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{ asset('fonts/iconic/css/material-design-iconic-font.min.css') }}">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{ asset('vendor/animate/animate.css') }}">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{ asset('vendor/css-hamburgers/hamburgers.min.css') }}">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{ asset('vendor/animsition/css/animsition.min.css') }}">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{ asset('vendor/select2/select2.min.css') }}">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{ asset('vendor/daterangepicker/daterangepicker.css') }}">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{ asset('css/util.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/main.css') }}">
    <!--===============================================================================================-->
</head>
<body style='background-image: url({{ asset('images/bg-01.jpg') }});background-size: cover;'>

<div class="container">
    <div class="row">
        <div class="col-md-12" style="margin-top: 15px;left:1%">
            <form class="custom_form register_form login100-form" action="{{ route('auth.register') }}" method="post">
                {{ csrf_field() }}
                <span class="login100-form-title p-b-49">
						{{ __('system_titles.register.title') }}
					</span>

                <div class="row">
                    <div class="col-md-6">
                        <div class="wrap-input100 validate-input m-b-23" data-validate="Full Name is required">
                            <span class="label-input100" style="float: left;{{
                             $errors->first('full_name') ? 'color : red' : ''
                            }}">Full Name</span>
                            <input class="input100" type="text" name="full_name" placeholder="Type your full name" value="{{ old('full_name') }}">
                            <span class="focus-input100" data-symbol="&#xf206;"></span>
                        </div>
                        @if ($errors->first('full_name'))
                        <div style="color: red;float: left;margin-top: -18px;margin-bottom: 20px;">{{ $errors->first('full_name') }}</div>
                        @endif
                    </div>
                    <div class="col-md-6">
                        <div class="wrap-input100 validate-input m-b-23" data-validate="Username is required">
                            <span class="label-input100" style="float: left;{{
                             $errors->first('username') ? 'color : red' : ''
                            }}">Username</span>
                            <input class="input100" type="text" name="username" placeholder="Type your username"
                                   autocomplete="false" readonly onfocus="this.removeAttribute('readonly');" value="{{ old('username') }}">
                            <span class="focus-input100" data-symbol="&#xf206;"></span>
                        </div>
                        @if ($errors->first('username'))
                            <div style="color: red;float: left;margin-top: -18px;margin-bottom: 20px;">{{ $errors->first('username') }}</div>
                        @endif
                    </div>

                    <div class="col-md-6">
                        <div class="wrap-input100 validate-input m-b-23" data-validate="Email is required">
                            <span class="label-input100" style="float: left;{{
                             $errors->first('email') ? 'color : red' : ''
                            }}">Email</span>
                            <input class="input100" type="text" name="email" placeholder="Type your email"
                                   autocomplete="false" readonly onfocus="this.removeAttribute('readonly');" value="{{ old('email') }}">
                            <span class="focus-input100" data-symbol="&#xf206;"></span>
                        </div>
                        @if ($errors->first('email'))
                            <div style="color: red;float: left;margin-top: -18px;margin-bottom: 20px;">{{ $errors->first('email') }}</div>
                        @endif
                    </div>
                    <div class="col-md-6">
                        <div class="wrap-input100 validate-input m-b-23" data-validate="Phone is required">
                            <span class="label-input100" style="float: left;{{
                             $errors->first('phone_number') ? 'color : red' : ''
                            }}">Phone</span>
                            <input class="input100" type="text" name="phone_number" placeholder="Type your phone"
                                   autocomplete="false" readonly onfocus="this.removeAttribute('readonly');" value="{{ old('phone_number') }}">
                            <span class="focus-input100" data-symbol="&#xf206;"></span>
                        </div>
                        @if ($errors->first('phone_number'))
                            <div style="color: red;float: left;margin-top: -18px;margin-bottom: 20px;">{{ $errors->first('phone_number') }}</div>
                        @endif
                    </div>

                    <div class="col-md-6">
                        <div class="wrap-input100 validate-input" data-validate="Password is required">
                            <span class="label-input100" style="float: left;{{
                                $errors->first('password') ? 'color : red' : ''
                            }}">Password</span>
                            <input class="input100" type="password" name="password" placeholder="Type your password"
                                   autocomplete="false" readonly onfocus="this.removeAttribute('readonly');">
                            <span class="focus-input100" data-symbol="&#xf190;" style="{{ $errors->first('password') ? 'color : red' : '' }}"></span>
                        </div>
                        @if ($errors->first('password'))
                            <div style="color: red;float: left">{{ $errors->first('password') }}</div>
                        @endif
                    </div>
                    <div class="col-md-6">
                        <div class="wrap-input100 validate-input" data-validate="Password Confirmation is required">
                            <span class="label-input100" style="float: left;{{
                             $errors->first('password') ? 'color : red' : ''
                            }}">Password Confirmation</span>
                            <input class="input100" type="password" name="password_confirmation" placeholder="Type your password again"
                                   autocomplete="false" readonly onfocus="this.removeAttribute('readonly');">
                            <span class="focus-input100" data-symbol="&#xf190;"></span>
                        </div>
                    </div>
                </div>

                <div class="container-login100-form-btn" style="margin-top: 35px">
                    <div class="wrap-login100-form-btn" style="width : 30% !important;">
                        <div class="login100-form-bgbtn"></div>
                        <button class="login100-form-btn register_form_btn" type="submit">
                            Sign Up
                        </button>
                    </div>
                </div>

                <div class="txt1 text-center p-t-54 p-b-20">
						<span>
							Or <a href="javascript:void(0)"><span style="color: #13cbe1;">Sign Up</span></a> Using
						</span>
                </div>

                <div class="flex-c-m">
                    <a href="{{ route('social.register','facebook') }}" class="login100-social-item bg1">
                        <i class="fa fa-facebook"></i>
                    </a>
                    <a href="{{ route('social.register','google') }}" class="login100-social-item bg3">
                        <i class="fa fa-google"></i>
                    </a>
                </div>
                <hr style="margin-top: 25px">
                <div class="flex-col-c p-t-155">
						<span class="txt1 p-b-17">
							Already have account ? <a class="login_btn" href="{{ route('auth.login.get') }}"><span style="color: #13cbe1;">Sign In</span></a>
						</span>
                </div>
            </form>
        </div>
    </div>
</div>


<div id="dropDownSelect1"></div>

<!--===============================================================================================-->
<script src="{{ asset('vendor/jquery/jquery-3.2.1.min.js') }}"></script>
<!--===============================================================================================-->
<script src="{{ asset('vendor/animsition/js/animsition.min.js') }}"></script>
<!--===============================================================================================-->
<script src="{{ asset('vendor/bootstrap/js/popper.js') }}"></script>
<script src="{{ asset('vendor/bootstrap/js/bootstrap.min.js') }}"></script>
<!--===============================================================================================-->
<script src="{{ asset('vendor/select2/select2.min.js') }}"></script>
<!--===============================================================================================-->
<script src="{{ asset('vendor/daterangepicker/moment.min.js') }}"></script>
<script src="{{ asset('vendor/daterangepicker/daterangepicker.js') }}"></script>
<!--===============================================================================================-->
<script src="{{ asset('vendor/countdowntime/countdowntime.js') }}"></script>
<!--===============================================================================================-->
<script src="{{ asset('js/jquery.mask.min.js') }}"></script>
<script src="{{ asset('js/main.js') }}"></script>

<script>
    $('input[name="phone_number"]').mask('+(994)00-000-00-00');
</script>
</body>
</html>
