<!DOCTYPE html>
<html lang="en">
<head>
    <title>Login System</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="{{ asset('vendor/bootstrap/css/bootstrap.min.css') }}">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{ asset('fonts/font-awesome-4.7.0/css/font-awesome.min.css') }}">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{ asset('fonts/iconic/css/material-design-iconic-font.min.css') }}">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{ asset('vendor/animate/animate.css') }}">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{ asset('vendor/css-hamburgers/hamburgers.min.css') }}">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{ asset('vendor/animsition/css/animsition.min.css') }}">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{ asset('vendor/select2/select2.min.css') }}">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{ asset('vendor/daterangepicker/daterangepicker.css') }}">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{ asset('css/util.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/main.css') }}">
    <!--===============================================================================================-->
</head>
<body style='background-image: url({{ asset('images/bg-01.jpg') }});background-size: cover;'>

<div class="container">
    <div class="row">
        <div class="col-md-6" style="left: 25%;margin-top:12px">
            <form class="custom_form login_form login100-form" method="post" action="{{ route('auth.login.post') }}">
                {{ csrf_field() }}
					<span class="login100-form-title p-b-49">
						Login System
					</span>

                <div class="wrap-input100 validate-input m-b-23" data-validate="Username is required">
                    <span class="label-input100" style="float: left;">Username or Email</span>
                    <input class="input100" type="text" name="username" placeholder="Type your username or email"
                           autocomplete="false" readonly onfocus="this.removeAttribute('readonly');">
                    <span class="focus-input100" data-symbol="&#xf206;"></span>
                </div>

                <div class="wrap-input100 validate-input" data-validate="Password is required">
                    <span class="label-input100" style="float: left;">Password</span>
                    <input class="input100" type="password" name="password" placeholder="Type your password"
                           autocomplete="false" readonly onfocus="this.removeAttribute('readonly');">
                    <span class="focus-input100" data-symbol="&#xf190;"></span>
                </div>

                <div class="text-right p-t-8 p-b-31">
                    <a href="#">
                        Forgot password?
                    </a>
                </div>
                @if (!$errors->isEmpty())
                    <div style="margin-bottom: 15px;color: red;">
                        <span>{{ $errors->first('error') }}</span>
                    </div>
                @endif
                <div class="container-login100-form-btn">
                    <div class="wrap-login100-form-btn">
                        <div class="login100-form-bgbtn"></div>
                        <button class="login100-form-btn" type="submit">
                            Sign In
                        </button>
                    </div>
                </div>

                <div class="txt1 text-center p-t-54 p-b-20">
						<span>
							Or <a href="javascript:void(0)"><span style="color: #13cbe1;">Sign In</span></a> Using
						</span>
                </div>

                <div class="flex-c-m">
                    <a href="{{ route('social.register','facebook') }}" class="login100-social-item bg1">
                        <i class="fa fa-facebook"></i>
                    </a>
                    <a href="{{ route('social.register','google') }}" class="login100-social-item bg3">
                        <i class="fa fa-google"></i>
                    </a>
                </div>
                <hr style="margin-top: 25px">
                <div class="flex-col-c p-t-155">
						<span class="txt1 p-b-17">
							Don't have an account ? <a class="register_btn" href="{{ route('auth.register.get') }}"><span
                                        style="color: #13cbe1;">Sign Up</span></a>
						</span>
                </div>
            </form>
        </div>
    </div>
</div>


<div id="dropDownSelect1"></div>

<!--===============================================================================================-->
<script src="{{ asset('vendor/jquery/jquery-3.2.1.min.js') }}"></script>
<!--===============================================================================================-->
<script src="{{ asset('vendor/animsition/js/animsition.min.js') }}"></script>
<!--===============================================================================================-->
<script src="{{ asset('vendor/bootstrap/js/popper.js') }}"></script>
<script src="{{ asset('vendor/bootstrap/js/bootstrap.min.js') }}"></script>
<!--===============================================================================================-->
<script src="{{ asset('vendor/select2/select2.min.js') }}"></script>
<!--===============================================================================================-->
<script src="{{ asset('vendor/daterangepicker/moment.min.js') }}"></script>
<script src="{{ asset('vendor/daterangepicker/daterangepicker.js') }}"></script>
<!--===============================================================================================-->
<script src="{{ asset('vendor/countdowntime/countdowntime.js') }}"></script>
<!--===============================================================================================-->
<script src="{{ asset('js/main.js') }}"></script>

</body>
</html>