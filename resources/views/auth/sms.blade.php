<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Sms Verification</title>
    <link rel="stylesheet" type="text/css" href="{{ asset('vendor/bootstrap/css/bootstrap.min.css') }}">
    <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
    <style>
        #wrapper {
            font-family: Lato, sans-serif;
            font-size: 1.5rem;
            text-align: center;
            box-sizing: border-box;
            color: #333;
        }

        #wrapper #dialog {
            border: solid 1px #ccc;
            margin: 10px auto;
            padding: 20px 30px;
            display: inline-block;
            box-shadow: 0 0 4px #ccc;
            background-color: #FAF8F8;
            overflow: hidden;
            position: relative;
            max-width: 450px;
        }

        #wrapper #dialog h3 {
            margin: 0 0 10px;
            padding: 0;
            line-height: 1.25;
        }

        #wrapper #dialog span {
            font-size: 90%;
        }

        #wrapper #dialog #form {
            max-width: 300px;
            margin: 25px auto 0;
        }

        #wrapper #dialog #form input {
            margin: 0 5px;
            text-align: center;
            line-height: 80px;
            font-size: 50px;
            border: solid 1px #ccc;
            box-shadow: 0 0 5px #ccc inset;
            outline: none;
            width: 20%;
            transition: all 0.2s ease-in-out;
            border-radius: 3px;
        }

        #wrapper #dialog #form input:focus {
            border-color: purple;
            box-shadow: 0 0 5px purple inset;
        }

        #wrapper #dialog #form input::selection {
            background: transparent;
        }

        #wrapper #dialog #form button {
            margin: 30px 0 50px;
            width: 100%;
            padding: 6px;
            background-color: #B85FC6;
            border: none;
            text-transform: uppercase;
        }

        #wrapper #dialog button.close {
            border: solid 2px;
            border-radius: 30px;
            line-height: 19px;
            font-size: 120%;
            width: 22px;
            position: absolute;
            right: 5px;
            top: 5px;
        }

        #wrapper #dialog div {
            position: relative;
            z-index: 1;
        }

        #wrapper #dialog img {
            position: absolute;
            bottom: -70px;
            right: -63px;
        }

    </style>
</head>
<body>
<div id="wrapper">
    <div id="dialog">
        <h3>Please enter the 4-digit verification code we sent via SMS:</h3>
        <span>We want to make sure it's you</span>
        <div id="form">
            <form action="{{ route('auth.sms.verify') }}" method="post">
                {{ csrf_field() }}
                <input type="text" maxLength="1" size="1" min="0" max="9" pattern="[0-9]{1}" name="number[]"/>
                <input type="text" maxLength="1" size="1" min="0" max="9" pattern="[0-9]{1}" name="number[]"/>
                <input type="text" maxLength="1" size="1" min="0" max="9" pattern="[0-9]{1}" name="number[]"/>
                <input type="text" maxLength="1" size="1" min="0" max="9" pattern="[0-9]{1}" name="number[]"/>
                <button type="submit" class="btn btn-primary btn-embossed" style="cursor: pointer;">Verify</button>
            </form>
        </div>

        <div>
            Didn't receive the code?<br/>
            <a href="#">Send code again</a><br/>
            <a href="#">Change phone number</a>
        </div>
    </div>
</div>
<script src="{{ asset('vendor/jquery/jquery-3.2.1.min.js') }}"></script>
<script src="{{ asset('vendor/bootstrap/js/popper.js') }}"></script>
<script src="{{ asset('vendor/bootstrap/js/bootstrap.min.js') }}"></script>
<script>
    $(function () {
        'use strict';

        var body = $('body');

        function goToNextInput(e) {
            var key = e.which,
                t = $(e.target),
                sib = t.next('input');

         // alert(key);

            if (key != 9 && (key < 48 || key > 57) &&
                key != 8 &&
                key != 103 && key != 104 && key != 105 &&
                key != 100 && key != 101 && key != 102 &&
                key != 96 && key != 97 && key != 98 && key != 99
            ) {
                e.preventDefault();
                return false;
            }

            if (key === 9) {
                return true;
            }

            if (!sib || !sib.length) {
                sib = body.find('input').eq(0);
            }
            sib.select().focus();
        }

        function onKeyDown(e) {
            var key = e.which;
           //alert(key);

            if (key === 9 || (key >= 48 && key <= 57) ||
                key === 8 ||
                key === 103 || key === 104 || key === 105 ||
                key === 100 || key === 101 || key === 102 ||
                key === 96 || key === 97 || key === 98 || key === 99
            ) {
                return true;
            }

            e.preventDefault();
            return false;
        }

        function onFocus(e) {
            $(e.target).select();
        }

        body.on('keyup', 'input', goToNextInput);
        body.on('keydown', 'input', onKeyDown);
        body.on('click', 'input', onFocus);

    })
</script>
</body>
</html>
