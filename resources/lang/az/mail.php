<?php
/**
 * Created by PhpStorm.
 * User: axmedbek
 * Date: 2/27/19
 * Time: 10:13 AM
 */

return [
    'subject' => 'Login Sistem Mail Təsdiq',
    'title' => 'Zəhmət olmasa aşağıdakı butona klikləyərək təsdiq edin',
    'verify_button' => 'Təsdiq et'
];