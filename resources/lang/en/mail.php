<?php
/**
 * Created by PhpStorm.
 * User: axmedbek
 * Date: 2/27/19
 * Time: 10:13 AM
 */

return [
    'subject' => 'Login System Verify Email Address',
    'title' => 'Please click the button below to verify your email address.',
    'verify_button' => 'Verify Email Address'
];