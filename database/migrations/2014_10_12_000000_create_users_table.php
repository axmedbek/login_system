<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('full_name')->nullable();
            $table->string('username')->nullable();
            $table->string('password')->nullable();
            $table->string('avatar')->nullable();

            $table->string('email')->unique()->nullable();
            $table->text('email_verification_code')->nullable();
           // $table->text('email_verification_code_activity_time')->nullable();
            $table->timestamp('email_verified_at')->nullable();

            $table->string('phone_number')->unique()->nullable();
            $table->timestamp('phone_number_verified_at')->nullable();
            $table->string('sms_verification_code')->nullable();
           // $table->string('sms_verification_code_activity_time')->nullable();

            $table->string('provider')->nullable();
            $table->string('provider_id')->nullable();
            $table->unsignedInteger('wrongLoginAttempts')->default(0);
            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
